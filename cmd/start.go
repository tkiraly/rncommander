package cmd

import (
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/jacobsa/go-serial/serial"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(startCmd)
}

//var macSetHex = regexp.MustCompile("mac set devaddr|deveui|appeui|nwkskey|appskey|appkey [0-9a-fA-F]{16}")
//var macSetNumber = regexp.MustCompile("mac set pwridx|dr|bat|retx|linkchk|rxdelay1|upctr|dnctr [0-9]+")
//var macSetState = regexp.MustCompile("mac set adr|ar on|off")
var macTxCommand = regexp.MustCompile("mac tx (cnf|uncnf) ([0-9]+) ([0-9a-fA-F]+)")
var macTxReply2 = regexp.MustCompile("mac_tx_ok|mac_rx [0-9]* [0-9]*")
var macJoinCommand = regexp.MustCompile("mac join (otaa|abp)")

var commands = []*regexp.Regexp{
	regexp.MustCompile("sys get ver"),
	regexp.MustCompile("mac get devaddr"),
	regexp.MustCompile("mac get deveui"),
	regexp.MustCompile("mac get appeui"),
	regexp.MustCompile("mac get dr"),
	regexp.MustCompile("mac get band"),
	regexp.MustCompile("mac get pwridx"),
	regexp.MustCompile("mac get adr"),
	regexp.MustCompile("mac get retx"),
	regexp.MustCompile("mac get rxdelay1"),
	regexp.MustCompile("mac get rxdelay2"),
	regexp.MustCompile("mac get ar"),
	regexp.MustCompile("mac get rx2 ([0-9]+)"),
	regexp.MustCompile("mac get dcycleps"),
	regexp.MustCompile("mac get mrgn"),
	regexp.MustCompile("mac get gwnb"),
	regexp.MustCompile("mac get status"),
	regexp.MustCompile("mac get sync"),
	regexp.MustCompile("mac get upctr"),
	regexp.MustCompile("mac get dnctr"),
	regexp.MustCompile("mac get ch freq ([0-9]+)"),
	regexp.MustCompile("mac get ch dcycle ([0-9]+)"),
	regexp.MustCompile("mac get ch drrange ([0-9]+)"),
	regexp.MustCompile("mac get ch status ([0-9]+)"),
	regexp.MustCompile("mac get class"),
	regexp.MustCompile("mac get mcast"),
	regexp.MustCompile("mac get mcastdevaddr"),
	regexp.MustCompile("mac get mcastdnctr"),
}
var replies = []*regexp.Regexp{
	regexp.MustCompile("RN2[0-9]{3} [0-9].[0-9].[0-9].[a-zA-Z]+ [0-9]+ [0-9]+ [0-9]+:[0-9]+:[0-9]+"),
	regexp.MustCompile("([0-9a-fA-F]{8})"),
	regexp.MustCompile("([0-9a-fA-F]{16})"),
	regexp.MustCompile("([0-9a-fA-F]{16})"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("(on|off)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("(on|off)"),
	regexp.MustCompile("([0-9]+) ([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9a-fA-F]{8})"),
	regexp.MustCompile("([0-9a-fA-F]{2})"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+)"),
	regexp.MustCompile("([0-9]+) ([0-9]+)"),
	regexp.MustCompile("(on|off)"),
	regexp.MustCompile("(A|C)"),
	regexp.MustCompile("(on|off)"),
	regexp.MustCompile("([0-9a-fA-F]{8})"),
	regexp.MustCompile("([0-9]+)"),
}

func printstatus(reply string) error {
	r, err := hex.DecodeString(reply)
	if err != nil {
		return err
	}
	fmt.Print("MAC state: ")
	switch r[3] & 0x0f {
	case 0:
		fmt.Print("Idle (transmissions are possible)")
	case 1:
		fmt.Print("Transmission occurring")
	case 2:
		fmt.Print("Before the opening of Receive window 1")
	case 3:
		fmt.Print("Receive window 1 is open")
	case 4:
		fmt.Print("Between Receive window 1 and Receive window 2")
	case 5:
		fmt.Print("Receive window 2 is open")
	case 6:
		fmt.Print("Retransmission delay - used for ADR_ACK delay, FSK can occur")
	case 7:
		fmt.Print("APB_delay")
	case 8:
		fmt.Print("Class C RX2 1 open")
	case 9:
		fmt.Print("Class C RX2 2 open")
	}
	fmt.Print("\n")
	fmt.Printf("Join status is ")
	if (r[3] & 0x10) == 0x10 {
		fmt.Printf("joined")
	} else {
		fmt.Printf("not joined")
	}
	fmt.Print("\n")
	fmt.Printf("Automatic reply is ")
	if (r[3] & 0x20) == 0x20 {
		fmt.Printf("enabled")
	} else {
		fmt.Printf("disabled")
	}
	fmt.Print("\n")
	fmt.Printf("ADR status is ")
	if (r[3] & 0x40) == 0x40 {
		fmt.Printf("enabled")
	} else {
		fmt.Printf("disabled")
	}
	fmt.Print("\n")
	fmt.Printf("Silent immediately status is ")
	if (r[3] & 0x80) == 0x80 {
		fmt.Printf("enabled")
	} else {
		fmt.Printf("disabled")
	}
	fmt.Print("\n")
	fmt.Printf("Mac pause status is ")
	if (r[2] & 0x01) == 0x01 {
		fmt.Printf("paused")
	} else {
		fmt.Printf("not paused")
	}
	fmt.Print("\n")
	fmt.Printf("Rx Done Status is ")
	if (r[2] & 0x02) == 0x02 {
		fmt.Printf("ready")
	} else {
		fmt.Printf("not ready")
	}
	fmt.Print("\n")
	fmt.Printf("Link check status is ")
	if (r[2] & 0x04) == 0x04 {
		fmt.Printf("enabled")
	} else {
		fmt.Printf("disabled")
	}
	fmt.Print("\n")
	fmt.Printf("Channels are ")
	if (r[2] & 0x08) == 0x08 {
		fmt.Printf("updated")
	} else {
		fmt.Printf("not updated")
	}
	fmt.Print("\n")
	fmt.Printf("Output power is ")
	if (r[2] & 0x10) == 0x10 {
		fmt.Printf("updated")
	} else {
		fmt.Printf("not updated")
	}
	fmt.Print("\n")
	fmt.Printf("NbRep is ")
	if (r[2] & 0x20) == 0x20 {
		fmt.Printf("updated")
	} else {
		fmt.Printf("not updated")
	}
	fmt.Print("\n")
	fmt.Printf("Prescaler is ")
	if (r[2] & 0x40) == 0x40 {
		fmt.Printf("updated")
	} else {
		fmt.Printf("not updated")
	}
	fmt.Print("\n")
	fmt.Printf("Second Receive window parameters is ")
	if (r[2] & 0x80) == 0x80 {
		fmt.Printf("updated")
	} else {
		fmt.Printf("not updated")
	}
	fmt.Print("\n")
	fmt.Printf("RX timing setup is ")
	if (r[1] & 0x01) == 0x01 {
		fmt.Printf("updated")
	} else {
		fmt.Printf("not updated")
	}
	fmt.Print("\n")
	fmt.Printf("Rejoin is ")
	if (r[1] & 0x02) == 0x02 {
		fmt.Printf("needed")
	} else {
		fmt.Printf("not needed")
	}
	fmt.Print("\n")
	fmt.Printf("Multicast status is ")
	if (r[1] & 0x04) == 0x04 {
		fmt.Printf("enabled")
	} else {
		fmt.Printf("disabled")
	}
	fmt.Print("\n")
	return nil
}

func process(command, reply string) (bool, bool) {
	for i := 0; i < len(commands); i++ {
		cm := commands[i].MatchString(command)
		rm := replies[i].MatchString(reply)
		if cm && rm {
			if command == "mac get status" {
				//printstatus(reply)

			}
			return true, false
		}
	}

	m := macJoinCommand.MatchString(command)
	if valid := strings.HasPrefix(reply, "ok"); valid && m {
		//return valid, true
		return valid, false
	}
	if valid := strings.HasPrefix(reply, "accepted"); valid && m {
		return valid, false
	}

	m = macTxCommand.MatchString(command)
	if valid := strings.HasPrefix(reply, "ok"); valid && m {
		//return valid, true
		return valid, false
	}
	if valid := macTxReply2.MatchString(reply); valid && m {
		return valid, false
	}
	if strings.HasPrefix(reply, "ok") {
		return true, false
	}
	logrus.Warningf("unknown command-reply combination: %s - %s\n", strings.TrimSpace(command), strings.TrimSpace(reply))
	return false, false
}

var lastcommand string
var startCmd = &cobra.Command{
	Use:   "start [serialport] [filename]",
	Short: "starts sending commands to RN",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			return fmt.Errorf("need at least 2 arguments")
		}
		next := make(chan bool, 1)
		errorChannel := make(chan error, 1)
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		contents, err := ioutil.ReadFile(args[1])
		if err != nil {
			return err
		}
		lines := strings.Split(string(contents), "\n")
		// Set up options.
		options := serial.OpenOptions{
			PortName:        args[0],
			BaudRate:        57600,
			DataBits:        8,
			StopBits:        1,
			MinimumReadSize: 1,
		}

		// Open the port.
		port, err := serial.Open(options)
		if err != nil {
			return err
		}

		// Make sure to close it later.
		defer port.Close()
		go func() {
			line := ""
			buffer := make([]byte, 32)
			for {
				n, err := port.Read(buffer)
				if err != nil {
					logrus.Fatalf("serial port read failed: %v", err)
					errorChannel <- err
				}
				for i := 0; i < n; i++ {
					if buffer[i] != '\n' {
						line += string(buffer[i])
					} else {
						line += string(buffer[i])
						reply := strings.TrimSpace(line)
						logrus.Infof("%s\n", reply)
						valid, waitformore := process(lastcommand, reply)
						if waitformore {
						} else if valid {
							next <- true
						} else {
							errorChannel <- fmt.Errorf("invalid reply: %s", line)
						}
						line = ""
					}
				}
			}
		}()
		next <- true
		sleepregex, err := regexp.Compile("sleep ([0-9]+)")
		if err != nil {
			return err
		}
		forregex, err := regexp.Compile("for ([0-9]+) ([0-9]+) (.+)")
		if err != nil {
			return err
		}
		for i := 0; i < len(lines); i++ {
			select {
			case <-c:
				logrus.Info("program will exit now")
				return nil
			case err := <-errorChannel:
				return err
			case <-next:
				strippedline := strings.TrimSpace(lines[i])
				if strings.HasPrefix(strippedline, "//") {
					next <- true
					continue
				} else if strings.HasPrefix(strippedline, "sleep") {
					matches := sleepregex.FindStringSubmatch(strippedline)
					if len(matches) == 2 {
						i, err := strconv.ParseInt(matches[1], 10, 64)
						if err != nil {
							return err
						}
						time.Sleep(time.Duration(i) * time.Millisecond)
						//continue
					} else {
						return fmt.Errorf("failed to parse %s", strippedline)
					}
					next <- true
					continue
				} else if strings.HasPrefix(strippedline, "for") {
					matches := forregex.FindStringSubmatch(strippedline)
					if len(matches) == 4 {
						iterations, err := strconv.ParseInt(matches[1], 10, 64)
						if err != nil {
							return err
						}
						sleep, err := strconv.ParseInt(matches[2], 10, 64)
						if err != nil {
							return err
						}
						logrus.Info("entering loop")
						for i := int64(0); i < iterations; i++ {
							send(port, matches[3])
							select {
							case <-c:
								logrus.Info("program will exit now")
								return nil
							case err := <-errorChannel:
								return err
							case <-time.After(time.Duration(sleep) * time.Millisecond):
								<-next //we can indeed go on
							}
						}
						logrus.Info("leaving loop")
						//continue
					} else {
						return fmt.Errorf("failed to parse %s", strippedline)
					}
					next <- true
					continue
				}
				send(port, strippedline)
			}
		}
		select {
		case <-c:
			logrus.Info("program will exit now")
			return nil
		case err := <-errorChannel:
			return err
		case <-next:
		}
		logrus.Info("program finished")
		return nil
	},
}

func send(port io.ReadWriteCloser, command string) error {
	b := []byte(command + "\r\n")
	_, err := port.Write(b)
	if err != nil {
		return err
	}
	lastcommand = command
	logrus.Info(command)
	return nil
}
