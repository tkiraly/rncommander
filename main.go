package main

import (
	"math/rand"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/tkiraly/rncommander/cmd"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	customFormatter := &logrus.TextFormatter{
		TimestampFormat: "15:04:05.999999",
		FullTimestamp:   true,
	}
	logrus.SetFormatter(customFormatter)
	//viper.SetEnvPrefix("RNCOMMANDER")
	//viper.AutomaticEnv()
	rand.Seed(time.Now().UnixNano())
	cmd.Execute()
}
